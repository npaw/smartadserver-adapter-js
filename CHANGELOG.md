## [6.7.2] 2020-07-16
### Fixed
- Skippable detection when having countdown

## [6.7.1] 2020-05-13
### Fixed
- Ad errors are now fatal

## [6.7.0] 2020-04-17
### Library
- Packaged with `lib 6.7.5`

## [6.5.0] 2019-10-08
### Library
- Packaged with `lib 6.5.16`
