var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.SmartAdServer = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    var ret = null
    if (this.playerContainer) {
      ret = this.playerContainer.currentTime
    }
    return ret
  },

  /** Override to return video duration */
  getDuration: function () {
    var ret = null
    if (this.playerContainer) {
      ret = this.playerContainer.duration
    }
    return ret
  },

  /** Override to return title */
  getTitle: function () {
    var ret = null
    if (this.actualAd && this.actualAd.inLine) {
      ret = this.actualAd.inLine.adTitle
    }
    return ret
  },

  /** Override to return resource URL. */
  getResource: function () {
    var ret = null
    if (this.playerContainer) {
      ret = this.playerContainer.currentSrc
    }
    return ret
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'SmartAdServer'
  },

  /* ONLY ADS
   * -----------------------
   */

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    return this.position
  },

  /** Override to return the given ad structure (list with number of pre, mid, and post breaks) (only ads) */
  getGivenBreaks: function () {
    var ret = 0
    var brk = this._getCurrentBreak()
    if (brk && brk.dirtyAds) {
      ret = brk.dirtyAds.length
    }
    return ret
  },

  /** Override to return the number of ads given for the break (only ads) */
  getGivenAds: function () {
    return 1
  },

  /** Override to return if the ad is being shown in the screen or not
   * The standard definition is: more than 50% of the pixels of the ad are on the screen
   * (only ads)
   */
  getIsVisible: function () {
    return youbora.Util.calculateAdViewability(this.playerContainer)
  },

  // /** Override to return a boolean showing if the audio is enabled when the ad begins (only ads) */
  getAudioEnabled: function () {
    if (this.player.video.getParam('publisher.muteAdsAtStartup')) {
      return false
    }
    return this.player.video.getParam('player.enableSound')
  },

  /** Override to return if the ad is skippable (only ads) */
  getIsSkippable: function () {
    return this.player.video.getParam('player.enableSkip') || this.player.video.getParam('player.enableCountdownSkip')
  },

  /** Override to return a boolean showing if the player is in fullscreen mode when the ad begins (only ads) */
  getIsFullscreen: function () {
    if (this.playerContainer && this.playerContainer.clientWidth && this.playerContainer.clientHeight) {
      return (window.innerHeight <= this.playerContainer.clientHeight + 30 && window.innerWidth <= this.playerContainer.clientWidth + 30)
    }
    return false
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, false)

    // References
    this.references = {
      adBegin: this.playListener.bind(this),
      adEnd: this.endedListener.bind(this),
      adError: this.errorListener.bind(this),
      singleError: this.errorListener.bind(this)
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.player.video && this.references) {
      for (var key in this.references) {
        this.player.video.removeCallback(key, this.references[key])
      }
      this.references = {}
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this._getPlayerContainer()
    if (e.state !== 'overlay') {
      switch (e.state) {
        case 'preroll':
          this.position = youbora.Constants.AdPosition.Preroll
          break
        case 'midroll':
          this.position = youbora.Constants.AdPosition.Midroll
          break
        case 'postroll':
          this.position = youbora.Constants.AdPosition.Postroll
          break
      }
      this.actualAd = this._getNextAd()
      if (!this.flags.isStarted) {
        this.fireStart()
        this._quartileTimer = new youbora.Timer(this.sendQuartile.bind(this), 500)
        this._quartileTimer.start()
      }
      if (!this.flags.isJoined) {
        this.fireJoin()
        this.lastDuration = this.getDuration()
      }
    }
  },

  sendQuartile: function (e) {
    var playhead = this.getPlayhead()
    if (playhead) {
      this.lastPlayhead = playhead
    }
    var quartile = 0
    if (playhead > this.lastDuration / 4) {
      if (!this.firstQuartile) {
        this.firstQuartile = true
        quartile = 1
      }
      if (playhead > this.lastDuration / 2) {
        if (!this.secondQuartile) {
          this.secondQuartile = true
          quartile = 2
        }
        if (playhead > this.lastDuration * 0.75) {
          if (!this.thirdQuartile) {
            this.thirdQuartile = true
            quartile = 3
          }
        }
      }
    }
    this.fireQuartile(quartile)
    if (quartile === 3) this._quartileTimer.stop()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError(e.errorCode)
    this.fireStop()
    this._resetValues()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    if (this.lastPlayhead + 1 > this.lastDuration) {
      this.fireStop({ adPlayhead: this.lastDuration })
    } else {
      this.fireSkip({ adPlayhead: this.lastPlayhead })
    }
    this._resetValues()
  },

  _getPlayerContainer: function () {
    if (!this.playerContainer) {
      this.playerContainer = null
      var base = document.getElementsByClassName('sas-container')
      if (base && base[0]) {
        var container = base[0]
        var ended = false
        while (!ended) {
          if (container.children.length > 0) {
            if (container.children[0].tagName === 'VIDEO') {
              this.playerContainer = container.children[0]
              ended = true
            } else {
              container = container.children[0]
            }
          } else {
            ended = true
          }
        }
      }
    }
  },

  _getBreaksList: function () {
    return this.player.video.VASTParser.getVASTParserInstance().store.getState().adPods
  },

  _getCurrentBreak: function () {
    var list = this._getBreaksList()
    var lastCompleted = null
    for (var index in list) {
      if (!lastCompleted) lastCompleted = list[index]
      if (list[index].stage !== 'USED') {
        lastCompleted = list[index]
      } else {
        return lastCompleted
      }
    }
    return lastCompleted
  },

  _getNextAd: function () {
    var brk = this._getCurrentBreak()
    if (brk && brk.dirtyAds) {
      if (!this.plugin.isBreakStarted) {
        return brk.dirtyAds[0]
      } else {
        var number = this.plugin.requestBuilder.lastSent.adNumber || 0
        return brk.dirtyAds[number]
      }
    }
    return null
  },

  _resetValues: function () {
    this.firstQuartile = false
    this.secondQuartile = false
    this.thirdQuartile = false
  }
})

module.exports = youbora.adapters.SmartAdServer
